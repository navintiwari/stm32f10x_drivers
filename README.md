/***

Readme file:
Author: Navin

***/

* This repo contains various drivers written by me for stm32 arm cortex m3 microcontrollers.
* Feel free to make changes and/or use them in your projects. 
* To use the drivers, you will need to include the "stm32f10x.h" header file and respective driver header files.
* These drivers are written independent of standard peripheral library. All the register level coding is visible here.

I'll be adding drivers for various peripherals and will try to make them as portable as possible across all stm32 cortex m3 devices.
Keep visiting.


keep it burning !
-Navin


